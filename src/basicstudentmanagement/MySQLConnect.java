/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicstudentmanagement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Vidya Katara
 */
public class MySQLConnect {
     private static final String CONNECTION_STRING="jdbc:mysql://localhost:3306/student_info";
    private static final String USERNAME="studylink";
    private static final String PASSWORD="studylink";
            
    
    /**
 * @ return Connection
 * Returns a valid connection to student_mgmt DB if it can, otherwise it returns null
 * 
 */
   public static Connection getConnection(){
       Connection conn = null;
       try{
           conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/student_info","studylink","studylink");
//            conn=DriverManager.getConnection(CONNECTION_STRING,USERNAME,PASSWORD );
           JOptionPane.showMessageDialog(null,"Connection Established");
       }catch(SQLException ex){
           JOptionPane.showMessageDialog(null,"Connection Failed:" + ex.getMessage());
       }
       return conn;
   }
   public static void main(String args[]){
       Connection con = getConnection();
   }
    
    
}
